#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <functional>

namespace gin {

    //  C O L O R
    struct color {
        float r{0}, g{0}, b{0}, a{1.0};

        color(float r, float g, float b, float a=1.0)
            : r(r), g(g), b(b), a(a) { }
        color(int v) : r(v), g(v), b(v) { }

        uint32_t rgb32();
        uint8_t gray8();
    };

    inline std::ostream& operator<<(std::ostream &os, gin::color const &o) {
        os << std::setprecision(4) << std::fixed << std::noshowpos;
        return os << "R: " << o.r << " G: " << o.g << " B: " << o.b;
    }


    //  R E C T
    struct rect {
        rect() { }
        rect(float x, float y, float w, float h)
            : x(x), y(y), w(w), h(h) { }

        float area() const { return w * h; }

        operator size_t() const { return area(); }

        bool operator==(const gin::rect& r) {
            return x == r.x && y == r.y && w == r.w && h == r.h;
        }
        bool operator!=(const gin::rect& r) { return !(*this == r); }

        rect integral() {
            return { std::floor(x), std::floor(y), std::ceil(w), std::ceil(h) };
        }

        float x = 0, y = 0, w = 0, h = 0;
    };

    inline std::ostream& operator<<(std::ostream &os, gin::rect const &o) {
        os << std::setprecision(4) << std::fixed << std::showpos;
        return os << "[" << o.x << "," << o.y << " " << o.w << "x" << o.h << "]";
    }


    struct cell;
    struct screen;

    //  R E N D E R E R
    struct renderer {

        virtual void attach(gin::screen& s);

        virtual void prep(gin::screen&) { }
        virtual void swap(gin::screen&) { }

        virtual void dirty(gin::cell& c);
        virtual void dirty(gin::rect& r);

        virtual void fillRect(gin::cell&) { }
        virtual void frameRect(gin::cell&) { }

        gin::rect area;
        std::vector< gin::rect > dirtys;

        bool valid{false};
        static gin::renderer& nil() { static gin::renderer _nil; return _nil; }
    };

    //  C E L L
    struct cell {

        cell() : up(*this), renderer(gin::renderer::nil()) {  }
        cell(std::string name) : up(*this), renderer(gin::renderer::nil()), name(name) { }

        bool         visible{true};

        std::string  name;
        gin::rect    pos;         // positioning, proportional to parent
        gin::rect    rect;        // computed relative to parent
        gin::rect    area;        // computed relative to screen
        gin::color   color{0};

        virtual void add(gin::cell& in, std::string name="");
        virtual void init();
        virtual void update();
        virtual void render() { };
        virtual void layout();

        void dirty() { painter().dirty(*this); }

        void moveTo(float x, float y);
        void sizeTo(float x, float y);

        gin::renderer& painter() {
            if (!child || renderer.get().valid)
                return renderer.get();
            return up.get().painter();
        }

        virtual void dump(int level=0);

        gin::cell& root();
        std::vector< std::reference_wrapper< gin::cell > > cells;
        std::reference_wrapper< gin::cell > up;
        std::reference_wrapper< gin::renderer> renderer;

        bool child{true};

        static gin::cell& nil() { static gin::cell _nil; return _nil; }
    };

    inline std::ostream& operator<<(std::ostream &os, gin::cell const &o) {
        return os << (!o.name.empty() ? ("\"" + o.name + "\" ") : "") 
                  << " %" << o.pos << " @" << o.area;;
    }


    //  S C R E E N
    struct screen : gin::cell {

        screen() { child = false; }

        virtual void init();
        virtual void draw();
        virtual void render();

        unsigned long context = 0;

        static gin::screen& nil() { static gin::screen _nil; return _nil; }
    };


    //  F I L L
    struct fill : gin::cell {
        fill() { color = 0; }
        fill(gin::color c, float w=0, float h=0) { color = c; rect.w = w; rect.h = h; }

        virtual void render() { painter().fillRect(*this);  }
    };


    //  F R A M E
    struct frame : gin::fill {
        frame() { color = 0; }
        frame(gin::color c, float w=0, float h=0) { color = c; rect.w = w; rect.h = h; }

        virtual void render() {
            painter().fillRect(*this);
            painter().frameRect(*this);
        }
    };

};
