#include "gin.h"

namespace gin {

    //  C O L O R
    uint32_t color::rgb32() {
        uint8_t r8 = (uint8_t) r * 0xFF;
        uint8_t g8 = (uint8_t) g * 0xFF;
        uint8_t b8 = (uint8_t) b * 0xFF;
        return (r8 << 16) | (g8 << 8) | b8;
    }
    uint8_t color::gray8() {
        return (uint8_t) 255.0 * ((0.3 * r) + (0.59 * g) + (0.11 * b));
    }

    //  C E L L
    void cell::add(cell& in, std::string name) {
        if (!name.empty()) in.name = name;
        in.up = *this;
        in.child = true;
        cells.push_back(in);
    }

    cell& cell::root() {
        return child ? up.get().root() : *this;
    }

    void cell::init() {
        layout();
        for (auto& cell : cells)
            cell.get().init();
    }

    void cell::update() {
        for (auto& cell : cells) {
            cell.get().update();
        }
    }

    void cell::moveTo(float x, float y) {
        //dirty();
        pos.x = x;
        pos.y = y;
        layout();
    }

    void cell::sizeTo(float w, float h) {
        //dirty();
        pos.w = w;
        pos.h = h;
        layout();
    }

    
    void cell::layout() {
        cell& parent = up.get();

        rect = { 
            pos.x * parent.pos.w,
            pos.y * parent.pos.h,
            pos.w, 
            pos.h 
        };

        area = {
            parent.area.x + (pos.x * parent.area.w),
            parent.area.y + (pos.y * parent.area.h),
            pos.w * parent.area.w, 
            pos.h * parent.area.h
        };

        for (auto& cell : cells) {
            cell.get().layout();
        }
    }

    void cell::dump(int level) {
        for (int i = 0; i < level; i++) std::cout << "  ";
        std::cout << *this << std::endl;
        for (auto& o : cells) {
            o.get().dump(level+1);
        }
        if (!child) std::cout << std::flush;
    }

    //  R E N D E R E R
    void renderer::attach(screen& s) {
        s.renderer = *this;
        area = s.area;
    }

    void renderer::dirty(cell& c) {
        dirty(c.rect);
    }

    void renderer::dirty(gin::rect& r) {
        dirtys.push_back(r);
    }

    //  S C R E E N
    void screen::init() {
        context = 1;
        cell::init();
    }

    void screen::draw() {
        painter().prep(*this);

        update();

        render();

        painter().swap(*this);
    }

    void screen::render() {
        for (auto& cell : cells) {
            cell.get().render();
        }
    }

}

