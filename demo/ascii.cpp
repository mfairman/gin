//
// gin ascii renderer demo
//

#include "gin/gin.h"

#include <iostream>
#include <chrono>
#include <thread>
#include <random>

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

using pixel = uint8_t;

// ASCII framebuffer renderer
struct : gin::renderer {

    void attach(gin::screen& s) {
        renderer::attach(s);
        grid.resize(area);
    }

    void fillRect(gin::cell& cell) {
        if (cell.color.a == 0) return;

        pixel v = cell.color.gray8();

        size_t rp = (size_t) (((int) cell.area.y * area.w) + cell.area.x);
        for (int row = 0; row < cell.area.h; row++) {
            for (int col = 0; col < cell.area.w; col++) {
                grid[rp+col] = v;
            }
            rp += (size_t) area.w;
        }
    }


    void frameRect(gin::cell& cell) {
        float h = cell.area.h;
        if (h <= 0) return;
        pixel v = 0xFF; // TBD selectable frame color

        size_t rp = (size_t) (((int) cell.area.y * area.w) + cell.area.x);

        for (int col = 0; col < cell.area.w; col++) {
            grid[rp+col] = v;
        }
        rp += (size_t) area.w;

        for (int row = 0; row < h-2; row++) {
            grid[rp] = v;
            grid[rp+cell.area.w] = v;
            rp += (size_t) area.w;
        }

        if (h > 1) {
            for (int col = 0; col < cell.area.w; col++) {
                grid[rp+col] = v;
            }
        }
    }

    void prep(gin::screen&) {
        memset(grid.data(), 0, grid.size());
        clear(); home();
        std::cout << "renderer area: " << area << std::endl;
    }

    void hborder() {
        std::cout << " +";
        for (int col = 0; col < area.w; col++)
            std::cout << "-";
        std::cout << "+" << std::endl;
    }

    void swap(gin::screen&) {
        hborder();
        size_t rp = 0;
        for (int row = 0; row < area.h; row++) {
            std::cout << " |";
            for (int col = 0; col < area.w; col++) {
                static const uint8_t chars[] = { ' ', '.', ':', 'o', 'O', '*' };
                pixel v = grid[rp+col];
                pixel i = ((int) v * sizeof(chars)) >> 8;
                std::cout << (char) chars[i];
            }
            std::cout << "|" << std::endl;
            rp += (size_t) area.w;
        }
        hborder();
        ++drawn;
    }

    void clear() {
        std::cout << "\033[2J" << std::flush;
    }

    void home() {
        std::cout << "\033[0;0H" << std::flush;
    }

    int drawn{0};

    std::vector< pixel > grid;
} ascii;


// screen for console terminal
struct console : gin::screen {
    console() {
        update();
    }

    void update() {
        // get terminal size
        struct winsize w;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

        // leave room for frame
        float s = w.ws_col - 4;
        gin::rect r{ 0, 0, s, (s / 5) - 2 };

        // update screen to terminal size
        if (area != r) {
            area = r.integral();
            pos = rect = { 0.0, 0.0, 1.0, 1.0 };
            ascii.attach(*this);
            name = "console(" + std::to_string((int) area.w) + "x"
                              + std::to_string((int) area.h) + ")";
        }


        gin::screen::update();
    }
} screen;

static std::random_device rd;
static std::mt19937 gen(rd());

struct {
    operator float() {
        std::uniform_real_distribution<> dis(0.0, 1.0);
        return dis(gen);
    }
} rando;

struct bouncer : gin::fill {

    float dx, dy;

    void init() {
        color = { rando, rando, rando };
        sizeTo(.02 + (rando/5), .02 + (rando/5));
        moveTo(rando/2, rando/2);
        dx = (rando - 0.5) * .1;
        dy = (rando - 0.5) * .05;
        name = "bouncer";

        gin::fill::init();
    }

    virtual void update() {
        dx += (rando - 0.5) * .005;
        dy += (rando - 0.5) * .005;
        moveTo(pos.x + dx, pos.y + dy);
        if (pos.x >= 1.0 - pos.w || pos.x <= 0) dx = -dx;
        if (pos.y >= 1.0 - pos.h || pos.y <= 0) dy = -dy;

        color.g += 0.0125;
        if (color.g > 1.0)
            color.g = .1;
        
        gin::fill::update();
    }
};

int main(int, char**) {

    bouncer b[9];
    for (size_t i = 0; i < sizeof(b)/sizeof(bouncer); i++)
        screen.add(b[i]);

    gin::frame a;
    a.sizeTo(.2, .2);
    a.moveTo(.1, .1);
    a.color = gin::color{ .3, .6, .4 };
    screen.add(a, "a-frame");

    screen.init();

    while (true) {

        screen.draw();
#if 1
        screen.dump();
#endif

        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    return 0;
}
